//
//  AccessibleUITestingUITests.swift
//  AccessibleUITestingUITests
//
//  Created by Rob Sturgeon on 18/11/2019.
//  Copyright © 2019 sturdysturge. All rights reserved.
//

import XCTest
import SwiftUI
class AccessibleUITestingUITests: XCTestCase {
    let app = XCUIApplication()
    override func setUp() {
        continueAfterFailure = false
        app.launch()
    }
    
    func testColouredButton() {
        //Check that logo is large
        XCTAssertEqual(app.images["logo"].label, "large logo", "Logo was not large by default")
        
        //The colours we expect
        let colours = [Color.red, Color.green, Color.blue]
        //Loop through the colours
        for colourIndex in 0...2 {
            if let colour = app.buttons["colour"].value as? String {
                //Check the label matches the colour we expect
                XCTAssertEqual(colour, colours[colourIndex].description, "The colour was wrong")
            }
            //Tap colour button
            app.buttons["colour"].tap()
            //Check that logo is still small
            XCTAssertEqual(app.images["logo"].label, "small logo", "The logo was not made small by the button")
        }
        
        if let colour = app.buttons["colour"].value as? String {
            //Check the label matches the colour we expect
            XCTAssertEqual(colour, colours[0].description, "The colour was wrong")
        }
        //Check that logo is still small
        XCTAssertEqual(app.images["logo"].label, "small logo", "The logo was not made small by the button")
    }
    func testSlider() {
        //Slide slider to 50% and check the label
        app.sliders["slider"].adjust(toNormalizedSliderPosition: 0.5)
        XCTAssertEqual(app.staticTexts["slider value"].label, "Slider: 0.5", "Slider's visible label was set to the wrong value")
        if let sliderValue = app.sliders["slider"].value as? String {
            XCTAssertEqual(sliderValue, "50%", "Slider's value was not the correct percentage")
        }
        
        //Slide slider to 0% and check the label
        app.sliders["slider"].adjust(toNormalizedSliderPosition: 0.0)
        XCTAssertEqual(app.staticTexts["slider value"].label, "Slider: 0.0", "Slider's visible label was set to the wrong value")
        if let sliderValue = app.sliders["slider"].value as? String {
            XCTAssertEqual(sliderValue, "0%", "Slider's value was not the correct percentage")
        }
        
        //Slide slider to 100% and check the label
        app.sliders["slider"].adjust(toNormalizedSliderPosition: 1.0)
        XCTAssertEqual(app.staticTexts["slider value"].label, "Slider: 1.0", "Slider's visible label was set to the wrong value")
        if let sliderValue = app.sliders["slider"].value as? String {
            XCTAssertEqual(sliderValue, "100%", "Slider's value was not the correct percentage")
        }
    }
    func testStepper() {
        //Increment stepper and check label
        var coordinate = app.otherElements["stepper"].coordinate(withNormalizedOffset: CGVector(dx: 0.9, dy: 0.5))
        coordinate.tap()
        XCTAssertEqual(app.staticTexts["stepper"].label, "Stepper: 1", "Stepper label did not display correct value")
        
        //Decrement stepper and check label
        coordinate = app.otherElements["stepper"].coordinate(withNormalizedOffset: CGVector(dx: 0.8, dy: 0.3))
        coordinate.tap()
        XCTAssertEqual(app.staticTexts["stepper"].label, "Stepper: 0", "Stepper label did not display correct value")
    }
    func testToggle() {
        //Check toggle label is empty at first
        XCTAssertEqual(app.switches["toggle"].label, "", "Toggle label was not empty when toggle is off")
        //Switch the toggle
        app.switches["toggle"].tap()
        //Check the toggle switch is now displayed
        XCTAssertEqual(app.switches["toggle"].label, "Toggle label", "Toggle label was not set when toggle is off")
    }
}
