//
//  Labels.swift
//  InterchangeUITests
//
//  Created by Rob Sturgeon on 19/10/2019.
//  Copyright © 2019 Rob Sturgeon. All rights reserved.
//
import XCTest

struct PropertyMatcher {
//The application being tested
    let app = XCUIApplication()
    
    enum Property {case label, title, value, placeholderValue, identifier}
    
    func getProperty(_ property: Property, from element: XCUIElement) -> String {
        switch property {
        case .label: return element.label
        case .title: return element.title
        case .value: return element.value as! String
        case .placeholderValue: return element.placeholderValue ?? ""
        case .identifier: return element.identifier
        }
    }
    
//Find all elements that match the query, and check the property matches the expected strings
    func checkAll(_ query: XCUIElementQuery, have property: Property, matching values: [String]) {
    //Loop through the expected values
        for value in values {
            //Predicate that the value matches the expected values
            let predicate = NSPredicate(format: "\(property) LIKE %@", value)
            //Check that it matches
            XCTAssertTrue(query.element(matching: predicate).exists, "Could not find \(property) \(value)")
        }
    }
    
//Get all labels for a given query, usually in order to make an array that can be searched later
    func getAll(_ query: XCUIElementQuery, by property: Property) -> [String] {
        //The array of string values to be returned
        var values = [String]()
        //The array of all elements matching the query
        let allElementsOfType = query.allElementsBoundByIndex
        
        //Loop through the elements
        for element in allElementsOfType {
        //Get the string for the property
        let string = getProperty(property, from: element)
        //Check if the string is empty
            if string.count > 0 {
                //Add the string because it isn't empty
                values.append(string)
            }
        }
        //Return the array of values
        return values
    }
}
