import SwiftUI

struct ContentView: View {
    
    @State var colour = Color.red
    @State var colourIndex = 0
    let colours = [Color.red, Color.green, Color.blue]
    @State var largeLogo = true
    @State var sliderValue = Double(0)
    @State var stepperValue = 0
    @State var toggleIsOn = false
    
    
    var body: some View {
        Form {
            Button(action: {
                if self.largeLogo {self.largeLogo = false}
                if (self.colourIndex + 1) < self.colours.count {self.colourIndex += 1}
                else {self.colourIndex = 0}
                self.colour = self.colours[self.colourIndex]
            }) {
                Rectangle()
                    .foregroundColor(colour)
            }
            .accessibility(identifier: "colour")
            .accessibility(label: Text("Next colour"))
            .accessibility(value: Text("\(colour.description)"))
            Image(largeLogo ? "large logo" : "small logo")
                .resizable()
                .scaledToFit()
                .accessibility(identifier: "logo")
            HStack {
                Text("Slider: \(String(format: "%.1f", Double(sliderValue)))")
                Slider(value: $sliderValue, label: {
                    Text("Slider: \(String(format: "%.1f", Double(sliderValue)))")
                })
                .accessibility(label: Text("slider"))
            }
            
            Stepper(value: $stepperValue, in: 0...10) {
                Text("Stepper: \(stepperValue)")
            }
            .accessibility(identifier: "stepper")
            Toggle(isOn: $toggleIsOn) {
                Text(toggleIsOn ? "Toggle label" : "")
            }
            .accessibility(label: Text(toggleIsOn ? "Toggle label" : ""))
            .accessibility(identifier: "toggle")
        }
    }
}
